# Statistical Learning for Environmental Changes

## **Overview**
The project, led by the **Max-Planck Institute for Biogeochemistry**, introduces the **YAXArraysToolbox.jl**, a Julia package tailored for handling large Earth Observation (EO) datasets. With increasing volumes of remote sensing data, this toolbox simplifies data analysis, visualization, and statistical driver attribution while seamlessly integrating with high-performance computing environments.

Contributors include **Daniel E. Pabon-Moreno**, **Gregory Duveiller**, **Markus Reichstein**, and **Alexander Winkler**. Funded by NFDI4Earth, the package aims to empower environmental data scientists with efficient tools for analyzing vast spatio-temporal datasets.

## **Features**
### **Core Components**
1. **Basic Operations**: Tools for manipulating and visualizing large gridded datasets, leveraging Julia's high-performance capabilities. Operations are designed with a user-friendly interface, akin to the R "Raster" package.
2. **Spatio-Temporal Analysis**:
   - *Space-for-Time Technique*: Evaluates local climate effects due to land-cover transitions, e.g., forest-to-savanna changes.
   - *Spatio-Temporal Cross-Validation*: Implements strategies like Leave-Location-Out (LLO) for unbiased model evaluation.
3. **Interoperability**: Works seamlessly with data cubes like those in the Earth System Data Laboratory (ESDL).

### **Technological Highlights**
- Built on the **YAXArrays.jl** backend for scalable operations.
- Integrated with Julia’s Makie ecosystem for advanced data visualization.
- Includes machine learning algorithms for forward feature selection and data-driven analyses.

## **Achievements**
- **Functional Release**: The toolbox is available on [GitHub](https://github.com/dpabon/YAXArraysToolbox.jl) under GPL-3 and archived on [Zenodo](https://doi.org/10.5281/zenodo.7989936).
- **Community Engagement**: Tutorials and examples for users to explore functionalities, available at [YAXArraysToolbox Tutorials](https://github.com/dpabon/YAXArraysToolboxNotebooks).
- **Case Study**: Demonstrated the space4time technique for land-cover impact on surface temperature across Africa, revealing temperature changes of 1-3 K due to forest-to-cropland transitions.

## **Challenges**
- Limited time prevented full implementation of planned features.
- Gaps in Julia’s statistics ecosystem slowed progress, but contributions to related packages improved robustness and functionality.

## **Future Directions**
1. **Feature Expansion**: Introduce advanced algorithms like Forward Feature Selection (FFS) and methods to estimate model applicability areas.
2. **Geospatial Enhancements**: Transition from Euclidean grids to equal-area hexagonal representations.
3. **Broad Ecosystem Integration**: Collaborate with projects like **Open Earth Monitor** to improve data handling capabilities globally.

## **Relevance**
This package addresses critical needs in the Earth system science community, offering tools that simplify the exploration of causal relationships in large datasets. Designed for students, researchers, and professionals, it opens doors to more efficient, scalable data analysis workflows in environmental science.

For more information, access the package's repository at [GitHub - YAXArraysToolbox](https://github.com/dpabon/YAXArraysToolbox.jl) and find tutorials on [GitHub Tutorials](https://github.com/dpabon/YAXArraysToolboxNotebooks).
